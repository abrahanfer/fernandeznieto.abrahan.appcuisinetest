//
//  ACTTableViewCell.h
//  FernandezNieto.Abrahan.AppCuisineTest
//
//  Created by Abrahán Fernández on 04/11/15.
//  Copyright (c) 2015 Abrahan Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACTTableViewCell : UITableViewCell

@property(nonatomic) UILabel* fibNumberLabel;

@property(nonatomic) UILabel* computingTimeLabel;

@end
