//
//  ACTTableViewCell.m
//  FernandezNieto.Abrahan.AppCuisineTest
//
//  Created by Abrahán Fernández on 04/11/15.
//  Copyright (c) 2015 Abrahan Fernandez. All rights reserved.
//

#import "ACTTableViewCell.h"

@implementation ACTTableViewCell

@synthesize fibNumberLabel, computingTimeLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        fibNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f, self.frame.size.width,
                                                               30.0f)];
        computingTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 20.0f, self.frame.size.width,
                                                                20.0f)];
        
        [computingTimeLabel setFont:[UIFont
                                    fontWithDescriptor:[UIFontDescriptor
                                                        preferredFontDescriptorWithTextStyle:
                                                        UIFontTextStyleBody]
                                    size:14.0f]];
        [computingTimeLabel setTextColor:[UIColor colorWithRed:162.0 / 255.0
                                                    green:162.0 / 255.0
                                                     blue:162.0 / 255.0
                                                    alpha:1]];
        
        [self.contentView addSubview:fibNumberLabel];
        [self.contentView addSubview:computingTimeLabel];
        
    }
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
