//-*- mode:objc -*-
//
//  ViewController.h
//  FernandezNieto.Abrahan.AppCuisineTest
//
//  Created by Abrahán Fernández on 04/11/15.
//  Copyright (c) 2015 Abrahan Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACTTableViewCell.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property(nonatomic) UITableView* fibTableView;

@property(nonatomic) ACTTableViewCell* fibCell;

- (void) calculateFibonacciNumber;

@end

