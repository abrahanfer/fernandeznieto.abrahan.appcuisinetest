//
//  ViewController.m
//  FernandezNieto.Abrahan.AppCuisineTest
//
//  Created by Abrahán Fernández on 04/11/15.
//  Copyright (c) 2015 Abrahan Fernandez. All rights reserved.
//

#import <limits.h>
#import <float.h>
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize fibTableView, fibCell;

double fibResultN;
double fibResultN_1;
NSTimeInterval computingTime;

NSMutableArray *resultsArray, *timeArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    resultsArray = [NSMutableArray array];
    timeArray = [NSMutableArray array];

    fibResultN = 1;
    fibResultN_1 = 0;
    [resultsArray addObject:[NSNumber numberWithDouble:fibResultN_1]];
    [resultsArray addObject:[NSNumber numberWithDouble:fibResultN]];
    [timeArray addObject:[NSNumber numberWithFloat:0.0f]];
    [timeArray addObject:[NSNumber numberWithFloat:0.0f]];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.view setAutoresizesSubviews:YES];
    //TODO: Make a new frame to center UITableView
    CGRect viewPosition = CGRectMake(0, 24, self.view.bounds.size.width, self.view.bounds.size.height);
    self.fibTableView = [[UITableView alloc] initWithFrame:viewPosition];
    [self.fibTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.fibTableView setDataSource:self];
    [self.fibTableView setDelegate:self];
    
    [self.view addSubview:self.fibTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    //Calculate the next fibonacci number
    int i = 2;
    while (fibResultN != 0) {
        //[self.fibTableView reloadData];
        //[self performSelectorInBackground:@selector(calculateFibonacciNumber) withObject:nil];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self calculateFibonacciNumber];
        });
        i++;
        NSLog(@"Results fib(%d) = %f in time: %f seconds.", i, fibResultN, fabs(computingTime));
    }
    fibResultN = fibResultN_1;
    //[self.fibTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"FibonacciCell";
    
    ACTTableViewCell *cell =
        [[ACTTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                               reuseIdentifier:CellIdentifier];
    
    double fibN = [(NSNumber *)[resultsArray objectAtIndex:indexPath.row] doubleValue];
    [cell.fibNumberLabel setText:[NSString stringWithFormat:@"%.0F",fibN]];
    [cell.computingTimeLabel setText:[NSString stringWithFormat:@"%f sec.",[(NSNumber *)[timeArray objectAtIndex:indexPath.row] doubleValue]]];
    
    if (fmodl(fibN,2) == 1) {
        [cell setBackgroundColor:[UIColor orangeColor]];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [resultsArray count];
}

- (void) calculateFibonacciNumber {
    double result;
    //Set in timer
    NSDate *startTime = [NSDate date];
    double aux = fibResultN_1;
    fibResultN_1 = fibResultN;
    if (fibResultN_1 > DBL_MAX - fibResultN) {
        result = 0;
    }else{
        result = fibResultN + aux;
    }
    //Set out timer
    computingTime = [startTime timeIntervalSinceNow];
    [resultsArray addObject:[NSNumber numberWithDouble:result]];
    [timeArray addObject:[NSNumber numberWithDouble:fabs(computingTime)]];
    
    fibResultN = result;
    
    [self.fibTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];

}

@end
