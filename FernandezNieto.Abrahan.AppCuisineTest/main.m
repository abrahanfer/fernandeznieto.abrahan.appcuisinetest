//
//  main.m
//  FernandezNieto.Abrahan.AppCuisineTest
//
//  Created by Abrahán Fernández on 04/11/15.
//  Copyright (c) 2015 Abrahan Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
